import cv2
import numpy as np
from PIL import Image
import os

""" Script for adding new images (faces) to neural network model """
data_path = 'dataset'

recognizer = cv2.face.LBPHFaceRecognizer_create()
detector = cv2.CascadeClassifier("haarcascade_frontalface_default.xml");


# get the images and label data
def get_images_and_labels(data_path):

    image_paths = [os.path.join(data_path, f) for f in os.listdir(data_path)]
    face_samples = []
    ids = []

    for imagePath in image_paths:

        # convert image to grayscale
        PIL_img = Image.open(imagePath).convert('L')
        img_numpy = np.array(PIL_img,'uint8')

        id = int(os.path.split(imagePath)[-1].split(".")[1])
        faces = detector.detectMultiScale(img_numpy)

        for (x, y, w, h) in faces:
            face_samples.append(img_numpy[y:y+h, x:x+w])
            ids.append(id)

    return face_samples, ids


print ("\n [INFO] Training faces. It will take a few seconds. Wait ...")
faces, ids = get_images_and_labels(data_path)

recognizer.train(faces, np.array(ids))

recognizer.save('trainer.yml')

print("\n [INFO] {0} faces trained. Exiting Program".format(len(np.unique(ids))))
