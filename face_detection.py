import cv2
import requests

""" Script for detecting faces using pre-trained neural network model with OpenCV """

recognizer = cv2.face.LBPHFaceRecognizer_create()
recognizer.read('trainer.yml')
cascadePath = "haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascadePath)

font = cv2.FONT_HERSHEY_SIMPLEX

cap = cv2.VideoCapture(0)

# set video width and height
cap.set(3, 640)
cap.set(4, 480)

while True:
    ret, frame = cap.read()
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    rects = faceCascade.detectMultiScale(frame, minSize=(30, 30))
    arg = ''
    
    for (x, y, w, h) in rects:
        predicted, conf = recognizer.predict(frame[y:y+h, x:x+w])
        print(conf)
        arg += str(predicted) + " " + str("{0:2.2f}".format(conf)) + '<br>'

        cv2.rectangle(frame, (x, y), (x+w, y+h), (255, 0, 0), 2)
        cv2.putText(frame, arg, (x, y+h), font, 0.8, (0, 255, 0), 2, cv2.LINE_AA)
        # print(r.status_code)

    r = requests.post("http://127.0.0.1:5000/detection", data={'send_name': arg})
    # cv2.imshow('camera', frame) 

    k = cv2.waitKey(10) & 0xff
    if k == 27:
        break

cap.release()
cv2.destroyAllWindows()
