from flask import Flask, render_template, request

app = Flask(__name__)

name = ""


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/detection')
def face_detect():
    return name


@app.route('/detection', methods = ['POST'])
def save_face():
    global name
    name = request.form['send_name']
    print(name)
    return 'success'


if __name__ == '__main__':
    app.run(debug=True)
