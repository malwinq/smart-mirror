# Smart mirror

Smart mirror project on Raspberry Pi with OpenCV and Flask.

Mirror recognizes faces (using neural network) and saves the log data. It displays info about weather and buses.

## Installation
```
sudo apt-get install -y libhdf5-dev libhdf5-serial-dev libatlas-base-dev libjasper-dev libqtgui4 libqt4-test
sudo pip3 install Flask
sudo pip3 install opencv-python
wget "https://raw.githubusercontent.com/Mjrovai/OpenCV-Face-Recognition/master/FaceDetection/Cascades/haarcascade_frontalface_default.xml"
```

## Running
```
export FLASK_APP=server.py
flask run
python3 gather_data.py
python3 trainer.py
python3 face_recognition.py
```
